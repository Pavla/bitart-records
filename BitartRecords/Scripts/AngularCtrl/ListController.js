﻿myapp.controller('ListController', ['$scope', '$http', '$modal', '$stateParams', function ($scope, $http, $modal, $stateParams) {

    $scope.RecordList = {};

    //dropdown item per page
    $scope.DropdownPerItem = {
        myOptions: [
              { value: '5' },
              { value: '10' },
              { value: '15' }
        ],
        selectedOption: { value: '10' } //This sets the default value of the select in the ui
    }

    $scope.currentPage = 1;     //default value

    callPaging();

    $scope.itPerPg = function () {

        callPaging();
    }


    $scope.deleteRecord = function (id) {

        alertify.dialog('confirm')
           .show()
           .set({ title: 'Delete record' })
           .set('defaultFocus', 'cancel')
           .set({
               'labels': { ok: 'Ok', cancel: 'Cancel' },
               'message': 'Are you sure you want to delete?',
               'onok': function () {

                   $.ajax({
                       url: '/api/Values/' + id,
                       type: 'DELETE',
                       contentType: 'application/json',

                       success: function () {
                           for (var i = 0; i < $scope.RecordList.length; i++) {
                               if ($scope.RecordList[i].RecordID === id) {
                                   $scope.RecordList.splice(i, 1);
                               }
                           }
                           callPaging();
                       },
                       error: function (data) {
                           alertify.error("Error with number id =" + id);
                       }
                   })
               },
               'oncancel': function () { }
               });
    };

    /////////////////////////////////////////////////////////

    $scope.RecordModal = function (record) {

        var modalInstance = $modal.open({
            templateUrl: 'Templates/ModalRecord.html',
            controller: 'ModalController',
            resolve: {
                selectedRecord: function () {                   //pass parameter to controller
                    return record;
                },
                allRecords: function () {                   //pass parameter to controller
                    return $scope.RecordList;
                },

                filterMonth: function () {                   //pass parameter to controller
                    return $scope.selectedMonth;
                },

                filterYear: function () {                   //pass parameter to controller
                    return $scope.selectedYear;
                },
            }
        }).result.then(function (caseAddEdit) {
            if (caseAddEdit == "add") {
                callPaging();
            }
        });

    }

    //////////////////////////////////////////////////////////

    $scope.filterMonthYear = function () {

        callPaging();

    }

    ///////////////////////PAIGING AND GET LIST OVER FACTORY ///////////////////////////////////////////

    function callPaging() {

        $scope.itemsPerPage = $scope.DropdownPerItem.selectedOption.value;

        var tempSelMonth;
        var tempSelYear;

        if ($scope.selectedMonth == undefined) {
            tempSelMonth = null;
        }
        else {
            tempSelMonth = $scope.selectedMonth.value;
        }

        if ($scope.selectedYear == undefined) {
            $scope.selectedYear = null;
        }     

        // get
        var filter = {
            month: tempSelMonth,
            year: $scope.selectedYear
        };

        $.ajax({
            url: '/api/Values/GetRecords',
            type: 'get',
            contentType: 'application/json',
            data: filter,
            success: function (data) {

                $scope.totalItems = data.length;
                $scope.$watch('currentPage + itemsPerPage', function () {
                    var begin = (parseInt($scope.currentPage) - 1) * parseInt($scope.itemsPerPage);
                    var end = parseInt(begin) + parseInt($scope.itemsPerPage);

                    $scope.RecordList = data.slice(begin, end);
                });
            },
            error: function (error) {
                alertify.alert("Error by getting record list")
                    .set({ transition: 'fade' })
                    .set({ title: '<span class="fa fa-times-circle fa-2x" ' + 'style="vertical-align:middle;color:#e10000;">' + '</span> Error' })
            }
        });
    }

}]);








