﻿myapp.directive('yeardropdown', function () {
    return {
        link: function (scope, element, attrs) {
            var year = new Date().getFullYear();
            var range = [];
            range.push(year);
            var yearDiff = year - 1900;     //from today year to 1990
            for (var i = 1; i < yearDiff + 1; i++) {
                range.push(year - i);
            }
            scope.years = range;
        },
    }
});


myapp.directive('monthdropdown', function () {
    return {      
        link: function (scope, element, attrs) {
            scope.months = [];
            scope.months.push({ value: 1, text: 'January' });
            scope.months.push({ value: 2, text: 'February' });
            scope.months.push({ value: 3, text: 'March' });
            scope.months.push({ value: 4, text: 'April' });
            scope.months.push({ value: 5, text: 'May' });
            scope.months.push({ value: 6, text: 'June' });
            scope.months.push({ value: 7, text: 'July' });
            scope.months.push({ value: 8, text: 'August' });
            scope.months.push({ value: 9, text: 'September' });
            scope.months.push({ value: 10, text: 'October' });
            scope.months.push({ value: 11, text: 'November' });
            scope.months.push({ value: 12, text: 'December' });
        },
    }
});


myapp.directive('datetimepicker', ['$timeout', function ($timeout) {
    return {
        require: '?ngModel',
        restrict: 'EA',
        scope: {
            datetimepickerOptions: '@',
            onDateChangeFunction: '&',
            onDateClickFunction: '&'
        },
        link: function ($scope, $element, $attrs, controller) {
            $element.on('dp.change', function () {
                $timeout(function () {
                    var dtp = $element.data('DateTimePicker');
                    controller.$setViewValue(dtp.date());
                    $scope.onDateChangeFunction();
                });
            });

            $element.on('click', function () {
                $scope.onDateClickFunction();
            });

            controller.$render = function () {
                if (!!controller && !!controller.$viewValue) {
                    var result = controller.$viewValue;
                    $element.data('DateTimePicker').date(result);
                }
            };

            $element.datetimepicker($scope.$eval($attrs.datetimepickerOptions));
        }
    };
}
]);
