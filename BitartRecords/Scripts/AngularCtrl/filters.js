﻿myapp.filter('datetime', function ($filter) {
    return function (input) {
        if (input == null) {
            return "";
        }
        var _date = $filter('date')(new Date(moment(input)), 'dd.MM.yyyy HH:mm:ss');
        return _date;

    };
});