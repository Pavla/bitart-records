﻿myapp.controller('FormController', ['$scope', '$http', '$stateParams', '$rootScope', function ($scope, $http, $stateParams, $rootScope) {

 
    $scope.start = function () {

        //disable/enable buttons
        $rootScope.isDisabled1 = true;
        $rootScope.isDisabled2 = false;

        //enter todays date in dateFrom
        $scope.dateFrom = moment().format('DD-MM-YYYY HH:mm:ss');

        //state of last record
        $rootScope.arrival = $scope.dateFrom;
    }

    $scope.stop = function () {

        //disable/enable buttons
        $rootScope.isDisabled1 = false;
        $rootScope.isDisabled2 = true;

        $scope.dateTo = moment().format('DD-MM-YYYY HH:mm:ss');

        //state of last record
        $rootScope.departure = $scope.dateTo;

    }

    $scope.saveForm = function () {
          
        if ($scope.dateFrom == undefined) {
            alertify.warning("Date/Time From is required");
        }
        else {
            if ($rootScope.isDisabled1 == true) {
                alertify.warning("Save function not working while Start mode is on");
            }

            else {
                    
                var postData = new Object();

                postData.DateFrom = moment.utc($scope.dateFrom, 'DD-MM-YYYY HH:mm:ss');
                postData.DateTo = moment.utc($scope.dateTo, 'DD-MM-YYYY HH:mm:ss');
                postData.Comment = $scope.comment;

                $http({
                    method: 'POST',
                    url: '/api/Values',
                    data: postData
                })
                .success(function (data) {

                    alertify.success("Saved");

                })

               .error(function () {
                   alertify.error("Error with adding new music");
               });

            }
        }
    }
   
}]);


