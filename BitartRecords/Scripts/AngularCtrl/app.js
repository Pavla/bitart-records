﻿var myapp = angular.module('RecordsApp', ['ui.bootstrap', 'ui.router', 'ngResource']);


myapp.run(function ($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;             // for ng-class={active: $state.includes()};
    $rootScope.$stateParams = $stateParams;


    //current date/time
    $rootScope.today = new Date();
    $rootScope.clock = {
        now: new Date()
    };

    var updateClock = function () {
        $rootScope.clock.now = new Date();
    };

    setInterval(function () {
        $rootScope.$apply(updateClock);
    }, 1000);
    updateClock();

    //enable/disable buttons
    $rootScope.isDisabled1 = false;
    $rootScope.isDisabled2 = true;

    //state of last record;
    $rootScope.arrival = "";
    $rootScope.departure = "";

});

myapp.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise("/");

    $stateProvider
            .state('records', {
                url: '/',
            })

            .state('form', {
                url: '/form',
                templateUrl: 'Templates/Form.html',
                controller: 'FormController'
            })

            .state('list', {
                url: '/list',
                templateUrl: 'Templates/List.html',
                controller: 'ListController'
            });

});


