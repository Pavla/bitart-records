﻿myapp.controller('ModalController', ['$scope', '$http', '$modal', '$modalInstance', 'selectedRecord', 'allRecords', 'filterMonth', 'filterYear', '$timeout', function ($scope, $http, $modal, $modalInstance, selectedRecord, allRecords, filterMonth, filterYear, $timeout) {

    //datepickerformat
    $scope.options = '{format:"DD.MM.YYYY HH:mm:ss"}'

    if (filterMonth == undefined) {
        filterMonth = null;
    }
    else {
        filterMonth = filterMonth.value;
    }

    var id;

    $scope.RecordList = allRecords;

    //add
    if (selectedRecord == undefined) {

        $scope.input = {
            DateFrom: "",
            DateTo: "",
            Comment: "",
        };

    }
    else {

        //edit

        $scope.input = {
            DateFrom: moment(selectedRecord.DateFrom).format("DD.MM.YYYY HH:mm:ss"),
            DateTo: moment(selectedRecord.DateTo).format("DD.MM.YYYY HH:mm:ss"),
            Comment: selectedRecord.Comment,
        };

        id = selectedRecord.RecordID;

    }


    $scope.SaveRecord = function () {


        //add
        if (selectedRecord == undefined) {

            var postData = new Object();
            postData.DateFrom = moment($scope.input.DateFrom).set('hour', moment($scope.input.DateFrom).get('hour') + 1);
            postData.DateTo = moment($scope.input.DateTo).set('hour', moment($scope.input.DateTo).get('hour') + 1);
            postData.Comment = $scope.input.Comment;

            $http({
                method: 'POST',
                url: '/api/Values',
                data: postData
            })
                .success(function () {
                    $modalInstance.close("add");
                    //$scope.RecordList.push(postData);

                })
               .error(function () {
                   $modalInstance.close("error");
                   alertify.error("Error with adding new record");
               });

        }
        else {
            //edit

            var postData = new Object();
            postData.DateFrom = moment($scope.input.DateFrom).set('hour', moment($scope.input.DateFrom).get('hour') + 1);
            postData.DateTo = moment($scope.input.DateTo).set('hour', moment($scope.input.DateTo).get('hour') + 1);
            postData.Comment = $scope.input.Comment;

            $http({
                method: 'PUT',
                url: '/api/Values/' + id,
                data: postData
            })
            .success(function (data) {
                if (data != null && data != "") {

 
                    for (var i = 0; i < $scope.RecordList.length; i++) {
                        if ($scope.RecordList[i].RecordID === id) {

                            //data.DateFrom = moment(data.DateFrom).set('hour', moment(data.DateFrom).get('hour') + 1);
                            //data.DateTo = moment(data.DateTo).set('hour', moment(data.DateTo).get('hour') + 2);

                            $scope.RecordList.splice(i, 1);
                            $scope.RecordList.push(data);
                        }
                    }
                }
                $modalInstance.close("edit");

            })

            .error(function (data, status, headers, config) {
                $modalInstance.close("error");
                alertify.error("Error with edit record");
            });
        }


    };

    $scope.Cancel = function () {

        $modalInstance.dismiss('cancel');

    };




}]);