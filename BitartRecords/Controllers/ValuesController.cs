﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BitartRecords.Classes;
using System.Collections;

namespace BitartRecords.Controllers
{

    public class ValuesController : ApiController
    {

        RecordsDatabaseEntities db = new RecordsDatabaseEntities();

   
        //// GET api/values
        //public IEnumerable<string> Get()
        //{
        //    return new string[] { "value1", "value2" };
        //}

        //// GET api/values/5
        //public string Get(int id)
        //{
        //    return "value";
        //}

        // GET api/values/GetRecords/5
        public IEnumerable<Records> GetRecords([FromUri]FilterModel filter)
        {

            if (filter.month != null && filter.year != null)
            {
                int month1 = Convert.ToInt32(filter.month);
                int year1 = Convert.ToInt32(filter.year);
                var RecordList = (from p in db.Records where p.DateFrom.Year == year1 && p.DateFrom.Month == month1 orderby p.DateFrom descending select p).ToList();
                return RecordList;
            }

            else if (filter.month == null && filter.year != null)
            {
                int year1 = Convert.ToInt32(filter.year);
                var RecordList = (from p in db.Records where p.DateFrom.Year == year1 orderby p.DateFrom descending select p).ToList();
                return RecordList;
            }

            else if (filter.month != null && filter.year == null)
            {
                int month1 = Convert.ToInt32(filter.month);
                var RecordList = (from p in db.Records where p.DateFrom.Month == month1 orderby p.DateFrom descending select p).ToList();
                return RecordList;
            }

            else
            {
                var RecordList = (from p in db.Records orderby p.DateFrom descending select p).ToList();
                return RecordList;
            }
        }

        // POST api/values
        public void Post([FromBody]Records postData)
        {
            db.Records.Add(postData);
            db.SaveChanges();
        }

        // PUT api/values/5
        public object Put(int id, [FromBody]Records postData)
        {
            var Record = (from p in db.Records where p.RecordID == id select p).FirstOrDefault();

            if (Record != null)
            {
                Record.DateFrom = Convert.ToDateTime(postData.DateFrom);
                //alowed to be null
                if (postData.DateTo != null)
                {
                    Record.DateTo = Convert.ToDateTime(postData.DateTo);
                }
                else
                {
                    Record.DateTo = null;
                }

                Record.Comment = postData.Comment;
                db.SaveChanges();
            }

            return Record;

        }

        // DELETE api/values/5
        public void Delete(int id)
        {
            var recordID = (from p in db.Records where p.RecordID == id select p).FirstOrDefault();

            if (recordID != null)
            {
                db.Records.Remove(recordID);
                db.SaveChanges();
            }
        }



       
    }
}